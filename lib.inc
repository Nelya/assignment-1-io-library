section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .count:
      cmp byte [rdi+rax], 0
      je .end_string
      inc rax
      jmp .count
    .end_string:
      ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    pop rsi
    mov  rdx, rax
    mov  rax, 1
    mov  rdi, 1
    syscall
    ret    

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rsi, rsp
    mov rax, 1
    mov rdx, 1
    mov rdi, 1
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    xor rax, rax
    mov rdi, 0xA
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov r8, rsp 
    mov rsi, 10 
    mov rax, rdi 
    mov rdi, rsp
    dec rdi
    sub rsp, 32
    mov byte[rdi], 0
    .division:
        xor rdx, rdx
        div rsi 
        add dl, '0'
        dec rdi
        mov [rdi], dl
        cmp rax, 0
        jnz .division
    call print_string
    add rsp, 32
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    xor rax, rax
    cmp rdi, 0
    jns print_uint
    push rdi
    mov rdi, '-' 
    call print_char
    pop rdi
    neg rdi
    call print_uint
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
    .check:
        mov dl, byte[rdi + rcx]
        mov dh, byte[rsi + rcx]
        cmp dl, dh
        jnz .not_equals
        inc rcx
        sub dl, 0
        jnz .check
    .equals:
        mov rax, 1
        jmp .end_equals
    .not_equals:
        xor rax, rax
    .end_equals:
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0
    mov rsi, rsp
    mov rdx, 1
    xor rax, rax
    xor rdi, rdi
    syscall
    cmp rax, 0
    js .end_read_char
    cmp rax, 1
    jz .end_read_char
    pop r10
    ret
    .end_read_char:
        pop rax
        ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    xor rax, rax
    push r12
    push r13
    push r14
    mov r12, rsi
    mov r13, rdi
    .check_symbols:
        xor r14, r14
        call read_char
        cmp rax, 0x20
        jz .check_symbols
        cmp rax, 0x9
        jz .check_symbols
        cmp rax, 0xA
        jz .check_symbols
        cmp rax, 0
        jz .epic_fail
        jmp .write
    .read:
        call read_char
        cmp rax, 0
        jz .victory_day
        cmp rax, 0x20
        jz .victory_day
        cmp rax, 0x9
        jz .victory_day
        cmp rax, 0xA
        jz .victory_day
    .write:
        mov byte[r13+r14], al
        inc r14
        cmp r14, r12 
        jz .epic_fail
        jmp .read
    .victory_day:
        mov byte[r13+r14], 0
        mov rax, r13
        mov rdx, r14
        jmp .end_reading
    .epic_fail:
        mov rax, 0
        mov rdx, 0
    .end_reading:
        pop r14
        pop r13
        pop r12
        ret
    
    
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rcx, rcx
    xor r8, r8
    mov r9, 10
    .read_first_digit:
        mov r8b, byte[rdi, rcx]
        sub r8b, 48
        jl .fail
        cmp r8b, 9
        ja .fail
        add rax, r8
        inc rcx
        cmp rax, 0
        jz .victory
    .read_digit:
        mov r8b, byte[rdi, rcx]
        sub r8b, 48
        jl .victory
        cmp r8b, 9
        ja .victory
        mul r9
        add rax, r8
        inc rcx
        jmp .read_digit
    .victory:
        mov rdx, rcx
        ret
    .fail:
        xor rdx, rdx
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    mov al, byte[rdi]
    cmp al, '-'
    jz .parse_minus
    cmp al, '+'
    jnz parse_uint
    inc rdi
    call parse_uint
    cmp rdx, 0
    jz .end
    inc rdx
    .end:
        ret 
    .parse_minus:
        inc rdi
        call parse_uint
        cmp rdx, 0
        jz .end
        neg rax
        inc rdx
        ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
    .copy:
        cmp rax, rdx
        jz .copy_fail
        mov r8b, [rdi+rax]
        mov byte[rsi+rax], r8b
        inc rax
        cmp byte[rsi+rax], 255
        jz .end_copy
        jmp .copy
    .copy_fail:
        xor rax, rax
    .end_copy:
        ret

